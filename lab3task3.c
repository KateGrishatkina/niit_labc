#include<stdio.h>
#include<string.h>
#define STRMAX 256

int main()
{
	struct {
		char* pWord;
		int lenWord;
	} words[STRMAX / 2] = { { 0, 0 } }, *pwords = words;
	char str[STRMAX];
	char* pstr = str;
	int lengthWord = 0, maxLenWord = 0, inWord = 0;
	puts("Enter your text (its maximum  length is 256 characters):");
	fgets(str, STRMAX, stdin);
	str[strlen(str) - 1] = 0;

	while (*pstr)
	{
		if (*pstr != ' ')
		{
			lengthWord++;
			if (!inWord)
			{
				inWord = 1;
				pwords->pWord = pstr;
			}

		}
		else if (inWord)
		{
			inWord = 0;
			pwords->lenWord = lengthWord;
			if (maxLenWord < lengthWord) maxLenWord = lengthWord;
			lengthWord = 0;
			pwords++;
		}
		pstr++;
	}
	if (inWord)
	{
		pwords->lenWord = lengthWord;
		if (maxLenWord < lengthWord) maxLenWord = lengthWord;
	}

	pwords = words;
	int i;
	printf("The longest words:\n");
	while (pwords->pWord)
	{
		if (pwords->lenWord == maxLenWord)
		{
			pstr = pwords->pWord;
			i = maxLenWord;
			while (i--)
			{
				putchar(*pstr);
				pstr++;
			}
			printf(" %d\n", maxLenWord);			
		}
		pwords++;
	}
	return 0;
}