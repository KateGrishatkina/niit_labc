#include <stdio.h>
#define _CRT_SECURE_NO_WARNINGS
#define length 21

int numberOfDigit(long int number)
{
	int count = 0;
	if (number < 0)
		number *= -1;
	while (number)
	{
		number /= 10;
		count++;
	}
	return count;
}

void extra_numberTostring(int numbDigit, long int number, char* str)
{
	*(str + numbDigit) = (char)((int)('0') + number % 10);
	if (0 == numbDigit) return;
	else
		extra_numberTostring(--numbDigit, number / 10, str);
}

void numberTostring(long int number, char* str)
{
	int numbOfDigit = 0;
	numbOfDigit = numberOfDigit(number);
	if (number < 0)
	{
		number *= -1;
		*str = '-';
		str++;
	}
	extra_numberTostring(numbOfDigit-1, number, str);
	*(str + numbOfDigit) = 0;
}

int main()
{
	char numbStr[length];
	long int number = 0;
	printf("Enter a integer number: ");
	if (!scanf_s("%d", &number))
	{
		puts("Error!");
		return 1;
	}

	numberTostring(number, numbStr);
	printf("\n");
	puts(numbStr);

	return 0;
}