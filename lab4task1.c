#include<stdio.h>
#include<string.h>
#define maxLenStr 80
#define maxNumbStr 20

typedef struct
{
	char* pstr;
	int length;
} descstr;

void swap(descstr* p1, descstr* p2)
{
	descstr temp;
	temp.pstr = p1->pstr;
	temp.length = p1->length;
	p1->pstr = p2->pstr;
	p1->length = p2->length;
	p2->pstr = temp.pstr;
	p2->length = temp.length;
}

void quickSort(descstr* str, int left, int right)
{
	int l = left;
	int r = right;
	int M = str[(left + right) / 2].length;

	do
	{
		while ((str[l].length < M) && (l < right))
			l++;
		while ((str[r].length > M) && (r > left))
			r--;
		if (l <= r)
		{
			swap(&str[l], &str[r]);
			l++;
			r--;
		}
	} while (l <= r);

	if (l < right) quickSort(str, l, right);
	if (left < r) quickSort(str, left, r);
}

int main()
{
	descstr strings[maxNumbStr];
	int i = 0;
	int j = 0;
	char buf[maxNumbStr][maxLenStr];
	int count = 0;
	puts("This program reads the strings util you write a empty string\n"
		"(Max number of string 20, Max number of characters in the string 80)\n"
		"Then it displays them in ascending length order");
	puts("Enter your text:");

	while ((*fgets(buf[i], 80, stdin) != '\n') && i<maxNumbStr)
	{
		buf[i][strings[i].length = strlen(buf[i]) - 1] = '\0';
		strings[i].pstr = buf[i];
		i++;
	}

	quickSort(strings, 0, i - 1);

	for (j=0; j < i; j++)
	{
		printf("%s\n", strings[j].pstr);
	}
	return 0;
}