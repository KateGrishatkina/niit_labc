#include<stdio.h>
#include<string.h>
#define MAXSTR 256

typedef struct
{
	char* beg;
	char* end;
} word;

int findWords(char* str, word* words)
{
	int inWord = 0;
	int countWords = 0;
	while (*str)
	{
		if (*str != ' '&&!inWord)
		{
			inWord = 1;
			words->beg = str;
			countWords++;
		}
		else if (*str == ' '&&inWord)
		{
			inWord = 0;
			words->end = str - 1;
			words++;
		}
		str++;
	}
	if (inWord) words->end = str - 1;
	return countWords;
}

void backwardsPrint(word* words, int numbWord)
{
	words += numbWord - 1;
	char *ch;
	while (numbWord)
	{
		ch = words->beg;
		while (ch <= words->end)
		{
			putchar(*ch);
			ch++;
		}
		putchar(' ');
		numbWord--;
		words--;
	}
	putchar('\n');
}



int main()
{
	char buf[MAXSTR];
	word words[MAXSTR / 2] = { { 0, 0 } };
	word* pwords = words;
	int numbWords;
	puts("Enter your text:");
	fgets(buf, MAXSTR, stdin);
	buf[strlen(buf) - 1] = '\0';
	numbWords = findWords(buf, words);
	backwardsPrint(pwords, numbWords);


	/*int i = 0;
	char *w;
	for (; i < numbWords; i++)
	{
		w = pwords->beg;
		while (w != (pwords->end+1))
		{
			putchar(*w);
			w++;
		}
		printf("\n");
		pwords++;
	}*/
	return 0;

}