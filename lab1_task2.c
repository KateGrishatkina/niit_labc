#include<stdio.h>
#define MAXNAME 80

int main()
{
	int hour, minute, second;
	char name[MAXNAME], answer;
	puts("What your name?");
	scanf_s("%s", name, MAXNAME);
	while (1)
	{
		while (1)
		{
			puts("What time is it (format HH:MM:SS)?");
			scanf_s("%d:%d:%d", &hour, &minute, &second);
			if (hour<0 || hour>23 || minute < 0 || minute >= 60 || second < 0 || second >= 60)
				puts("Error! Incorrect data!");
			else break;
		}
		//Nighttime - 23:01-03:59 (lasting 4 hours 59 minutes) Morning - 04:00-11:00 (lasting 7 hours 1 minute) 
		//Daytime - 11:01-16:00 (lasting 5 hours) Evening - 16:01-23:00 (lasting 7 hours) 
		if ((hour == 23 && (second >= 1 && second < 60)) || ((hour >= 0 && hour <= 3) && (second >= 0 && second < 60)))
			printf("%s, it is nighttime! Wish you sweet dreams!\n", name);
		else if (((hour >= 4 && hour < 11) && (second >= 0 && second < 60)) || (hour == 11 && minute == 0 && second == 0))
			printf("Good morning, %s!\n", name);
		else if ((hour == 11 && (second >= 1 && second < 60)) || ((hour>11 && hour < 16) && (second >= 0 && second < 60)) || (hour == 16 && minute == 0 && second == 0))
			printf("Good afternoon, %s!\n", name);
		else if ((hour == 16 && (second >= 1 && second < 60)) || ((hour>16 && hour < 23) && (second >= 0 && second < 60)) || (hour == 23 && minute == 0 && second == 0))
			printf("Good evening, %s!\n", name);
		fflush(stdin);
		puts("To continue, press C");
		scanf_s("%c", &answer, 1);
		if (answer != 'C') return 0;	
	}

	return 0;
}