#include<stdio.h>
#include<string.h>
#define MAXSTR 256

int main()
{
	char buf[MAXSTR];
	char* pbuf = buf;
	int inWord = 0, wordCounter = 0, userNumbWord = 1, inNWord = 1;
	puts("This programm output Nth word of string.\nEnter your text (its maximum  length is 255 characters):");
	fgets(buf, MAXSTR, stdin);
	buf[strlen(buf)] = 0;
	fflush(stdin);
	printf("Enter a number of word: ");
	if (!scanf_s("%d", &userNumbWord))
	{
		puts("Incorrect date!");
		return 0;
	}
    // pointers
	while (*pbuf && inNWord)
	{
		if (*pbuf != ' '&& !inWord)
		{
			inWord = 1;
			if (++wordCounter == userNumbWord)
			{
				inNWord = 0;
				continue;
			}
		}
		else if (*pbuf == ' ' && inWord)
			inWord = 0;
		pbuf++;
	}

	if (inNWord)
		printf("The string don't have the %dth word", userNumbWord);
	else
	{
		printf("The %dth word: ", userNumbWord);
		while (*pbuf && *pbuf != ' ')
		{
			putchar(*pbuf);
			pbuf++;
		}
	}

	// []
	int i = 0; 
	inNWord = 1; 
	wordCounter = 0;
	inWord = 0;
	while (buf[i] && inNWord)
	{
		printf("i in while %d\n", i);
		if (buf[i] != ' '&& !inWord)
		{
			inWord = 1;
			if (++wordCounter == userNumbWord)
			{
				inNWord = 0;
				continue;
			}
		}
		else if (buf[i] == ' ' && inWord)
			inWord = 0;
		i++;
	}

	if (inNWord)
		printf("The string don't have the %dth word", userNumbWord);
	else
	{
		printf("The %dth word: ", userNumbWord);
		while (buf[i] && buf[i] != ' ')
		{
			putchar(buf[i]);
			i++;
		}
	}

	putchar('\n');
	return 0;
}