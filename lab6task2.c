/*


The longest  Collatz sequence


*/
#include<stdio.h>
unsigned int seqCollatz_extra(unsigned int number, unsigned int count)
{
	if (1 == number) return count;
	else
	{
		count++;
		//printf("count = %d\n", count);
		if (number % 2) seqCollatz_extra(3 * number + 1, count);
		else seqCollatz_extra(number / 2, count);
	}
}

unsigned int seqCollatz(unsigned int number)
{
	unsigned int count = 1;
	seqCollatz_extra(number, count);
	//printf("number = %d, count = %d\n", number, count);
	return seqCollatz_extra(number, count);
}


int main()
{
	unsigned int maxlenSeq = 0;
	unsigned int templen = 0;
	unsigned int number_maxlen = 2;
	unsigned int i = 2;
	for (; i <= 1000000; i++)
	{
		templen = seqCollatz(i);
		if (maxlenSeq < templen)
		{
			maxlenSeq = templen;
			number_maxlen = i;
		}
	}
	printf("The longest  Collatz sequence (number %d): %d\n", number_maxlen, maxlenSeq);
	return 0;
}