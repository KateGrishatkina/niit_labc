#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#define maxLenStr 80
#define maxNumbStr 20

typedef struct
{
	char str[maxLenStr];
	int length;
} descstr;

void myStrcopy(char* contstr, char* str)
{
	while (*contstr = *str)
	{
		contstr++;
		str++;
	}
}

void swap(descstr* p1, descstr* p2)
{
	int i = 0;
	descstr temp;
	myStrcopy(temp.str,p1->str);
	temp.length = p1->length;
	myStrcopy(p1->str, p2->str);
	p1->length = p2->length;
	myStrcopy(p2->str, temp.str);
	p2->length = temp.length;
}

void quickSort(descstr* str, int left, int right)
{
	int l = left;
	int r = right;
	int M = str[(left + right) / 2].length;

	do
	{
		while ((str[l].length < M) && (l < right))
			l++;
		while ((str[r].length > M) && (r > left))
			r--;
		if (l <= r)
		{
			swap(&str[l], &str[r]);
			l++;
			r--;
		}
	} while (l <= r);

	if (l < right) quickSort(str, l, right);
	if (left < r) quickSort(str, left, r);
}

int readFile(char* filename, descstr* strings)
{
	errno_t err;
	int count=0;
	FILE* file;
	err = fopen_s(&file, filename, "r");
	if (err !=0)
	{
		perror("File Error!");
		exit(1);
	}
	while ((fgets(strings->str, maxLenStr, file)) && count < maxNumbStr)
	{
		count++;
		strings->str[strlen(strings->str) - 1] = '\0';
		strings->length = strlen(strings->str);
		strings++;
	}
	fclose(file);
	return count;
}

void printFile(descstr* strings, int numbstr)
{
	FILE *file;
	errno_t err;
	err = fopen_s(&file, "output.txt", "wt");
	if (err != 0)
	{
		perror("File Error!");
		exit(1);
	}
	while (numbstr)
	{
		fputs(strings->str, file);
		fputc('\n', file);
		strings++;
		numbstr--;
	}
	fclose(file);	
}

int main()
{
	char name[maxLenStr];
	descstr strings[maxNumbStr];
	int i = 0;
	int j = 0;
	int count = 0;
	puts("This program reads the strings from file\n"
		"(Max number of string 20, Max number of characters in the string 80)\n"
		"Then it prints them in fail in ascending length order");
	puts("Enter name of fail. Max length is 80 characters");
	fgets(name, maxLenStr, stdin);
	name[strlen(name) - 1] = '\0'; 
	count = readFile(name, strings);
	quickSort(strings, 0, count-1);
	printFile(strings, count);
	return 0;
}