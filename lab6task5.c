#define _CRT_SECURE_NO_WARNINGS

#include<stdio.h>
#include<stdlib.h>
#include<time.h>

int fibonacciNumber(int n)
{
	int i = 0;
	int temp = 0;
	int needed_member = 1;
	for (; i <= n; i++)
	{
		needed_member += temp;
		temp = needed_member - temp;
	}
	return needed_member;
}

int fibonacciNumber_rec(int n)
{
	if (n < 1) return 1;
	else return(fibonacciNumber_rec(n - 1) + fibonacciNumber_rec(n - 2));
}

int main()
{
	int number = 0; 
	int member = 0;
	FILE* fout;
	int i = 1;
	long j = 0;
	time_t start, stop;

	printf("Which member of Fibonacci sequence is calculated(max 40)? ");
	if (!scanf_s("%d", &number) || number < 1 || number>40)
	{
		printf("Error!");
		return 1;
	}
	fout = fopen("fout.txt", "wt");
	printf("iterative\n");
	fprintf(fout, "iterative\n");
	for (i = 0; i <= number; i++)
	{
		start = time(NULL);
		for (j=0; j < 50000000; j++)
			member = fibonacciNumber(i);
		stop = time(NULL);
		printf("number=%d member=%d time=%ld\n", i, member, stop - start);
		fprintf(fout, "number=%d member=%d time=%d\n", i, member, stop - start);
	}
	printf("-----------------------------------------------------------\n");
	fprintf(fout, "-----------------------------------------------------------\n");
	printf("recursive\n"); 
	fprintf(fout, "recursive\n");
	for (i = 0; i <= number; i++)
	{
		start = time(NULL);
		for (j = 0; j < 50000000; j++)
			member = fibonacciNumber_rec(i);
		stop = time(NULL);
		printf("number=%d member=%d time=%ld\n", i, member, stop - start);
		fprintf(fout, "number=%d member=%d time=%d\n", i, member, stop - start);
	}
	fclose(fout);
	return 0;
}