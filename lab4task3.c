#include<stdio.h>
#include<string.h>
#define STRMAX 80

int IsPalindrome(char*);

int main()
{
	char buf[STRMAX];
	puts("This programm determines a input string is palindrome or not.\n"
		"Enter your text(max length is 79 characters)\n");
	fgets(buf, STRMAX, stdin);
	buf[strlen(buf)-1] = '\0';
	if (IsPalindrome(buf))
		puts("\nIt is palindrome!");
	else
		puts("\nIt isn't palindrome");
	return 0;
}

int IsPalindrome(char *str)
{
	char *end = str + strlen(str) - 1;
	while (str < end)
	{
		if (*str != *end) return 0;
		str++;
		end--;
	}
	return 1;
}