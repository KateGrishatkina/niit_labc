#include<stdio.h>
#define MAXSTR 256

int lenSequence(char* firstCh)
{

	int counter=1;
	if (*firstCh == *(firstCh + 1)) counter += lenSequence(++firstCh);
	return  counter;
}

int main()
{
	struct {
		char* pSeq;
		int lenSeq;
	} sequence[MAXSTR] = { { 0, 0 } }, *psequence=sequence;
	char str[MAXSTR];
	char* pstr = str;
	int lenLongestSeq = 1;

	puts("This programm find the longest sequence of characters in the text.\nEnter a text (its maximum  length is 256 characters):");
	gets_s(str, MAXSTR - 1);
	while (*pstr)
	{
		psequence->pSeq = pstr;
		psequence->lenSeq = lenSequence(pstr);
		if (lenLongestSeq < psequence->lenSeq) lenLongestSeq = psequence->lenSeq;
		pstr += psequence->lenSeq;
		psequence++;
	}

	printf("\nLength of the longest sequence: %d\n", lenLongestSeq);
	int i;	
	psequence = sequence;
	
	while (psequence->lenSeq)
	{
		if (psequence->lenSeq == lenLongestSeq)
		{
			i = lenLongestSeq;
			pstr = psequence->pSeq;
			while (i)
			{
				printf("%c", *pstr);
				pstr++;
				i--;
			}
			printf("\n");
		}
		psequence++;
	}
	
	return 0;
}