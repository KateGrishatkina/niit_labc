#include <stdio.h>
#define NAME_LENGTH 80
float BMI(int height, int weight)
{
	return (float)weight * 10000.0/((float)height*(float)height);
}

int main()
{
	int height, weight;
	char gender;
	char name[NAME_LENGTH];
	char mes[][45] = { "Underweight! You need to gain weight!",
		"Normal (healthy weight)! You are fine!",
		"Overweight! �ou need to lose weight",
		"Moderately obese! You need to lose weight",
		"Severely obese! You need to lose weight" };
	float index;


	puts("What is your name? ");
	scanf_s("%s", name, NAME_LENGTH);
	printf("Hello, %s!\n", name);
	while (1)
	{
		fflush(stdin);
		printf("What is your gender: M/W? ", name);
		scanf_s("%c", &gender, 1);
		if (gender !=77 && gender != 87)
			puts("Error! Incorrect data!");
		else break;
	}
	printf("Good, %s! What is your height in cm, weight in kg? ", name);
	scanf_s("%d%d", &height, &weight);
	index = BMI(height, weight);
	if (gender == 77)
	{
		if (index < 20.0) puts(mes[0]);
		else if (index >= 20.0&&index < 25.0) puts(mes[1]); 
		else if (index >= 25.0&&index < 30.0) puts(mes[2]); 
		else if (index >= 30.0&&index < 40.0) puts(mes[3]); 
		else if (index >=40.0) puts(mes[4]);
	}
	else
	{
		if (index < 19.0) puts(mes[0]);
		else if (index >= 19.0&&index < 24.0) puts(mes[1]);
		else if (index >= 24.0&&index < 30.0) puts(mes[2]);
		else if (index >= 30.0&&index < 40.0) puts(mes[3]);
		else if (index >= 40.0) puts(mes[4]);
	}

	return 0;
}