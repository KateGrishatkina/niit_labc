#include<stdio.h>
const char character = '*';

int answerToContinue()
{
	char answer;
	fflush(stdin);
	printf("\nTo continue press C: ");
	scanf_s("%c", &answer, 1);
	if ('C' == answer || 'c' == answer) return 1;
	else return 0;
}

int dataInputValidation(int *number)
{
	fflush(stdin);
	if (!scanf_s("%d", number) || *number < 1 || *number > 40)
	{
		printf("Error! Uncorrect data! ");
		if (answerToContinue()) dataInputValidation(number);
		else return 1;
	}
	else return 0;
}

void drowTriangleRecursion(int numberRow, int numbOfRows)
{
	int numberOfCharacter = 2 * (numberRow - 1);
	if (0 == numberRow)
	{
		printf("\n");
		return;
	}
	drowTriangleRecursion(numberRow - 1, numbOfRows);
	printf("%*c", numbOfRows - numberRow + 1, character);
	while (numberOfCharacter--)
		printf("%c", character);
	printf("\n");
}

int main()
{
	int numbOfRows=0;
	int numberRow = 1;
	printf("Enter the height of the triangle (number of rows, min 1, max 40): ");
	if(dataInputValidation(&numbOfRows)) return 0;
	//feature: number of charaters for a current row can be calculeted as ((number of current row - 1 )*2 + 1)
	for (numberRow; numberRow <= numbOfRows; numberRow++)
	{
		int numberOfCharacter = 2 * (numberRow-1);
		printf("%*c", numbOfRows - numberRow + 1, character);
		while (numberOfCharacter--)
			printf("%c", character);
		printf("\n");
	}
	drowTriangleRecursion(numbOfRows, numbOfRows);
	fflush(stdin);
	getchar();
	return 0;
}

