#include<stdio.h>
#include<stdlib.h>
#include<time.h>

char randomCharacter()
{
	char ch = '0';
	//srand((int)time(0));
	switch (rand() % 3)
	{
	case 0:
		ch = rand() % 10 + '0';
		break;
	case 1:
		ch = rand() % ('Z' - 'A') + 'A';
		break;
	case 2:
		ch = rand() % ('z' - 'a') + 'a';
		break;
	}
	return ch;
}

int main()
{
	char randCh;
	int i, j;
	puts("The program prints ten passwords(the length is 8 characters) on the screen. They consists of random characters of the Latin alphabet and of numbers.");
	srand((int)time(0));
	for (i = 1; i <= 10; i++)
	{
		printf("\t"); // better seen
		for (j = 1; j <= 8; j++)
			putchar(randomCharacter());
		printf("\n");
	}

	getchar();
	return 0;
}