#include<stdio.h>
#define MAXSTR 256

int main()
{
	char string[MAXSTR];
	int tableFrequencyCh[2][MAXSTR+1] = { { 0 } };
	int i=0, j=0;
	puts("Enter text (maximum string length is 256 characters):");
	gets_s(string, MAXSTR - 1);
	for (; string[i]; i++)
	{
		for (j=0; tableFrequencyCh[0][j]; j++)
		{
			if ((int)string[i] == tableFrequencyCh[0][j])
			{
				tableFrequencyCh[1][j]++;
				break;
			}
		}
		if (!tableFrequencyCh[0][j])
		{
			tableFrequencyCh[0][j] = (int)string[i];
			tableFrequencyCh[1][j]++;
		}
	}
	for (i =0; tableFrequencyCh[0][i*9] && i<26; i++)
	{
		for (j = 0; tableFrequencyCh[0][i*9+j] && j < 9; j++)
			printf("%3c\t", (char)tableFrequencyCh[0][i*9+j]);
		printf("\n");
		for (j = 0; tableFrequencyCh[1][i*9+j] && j < 9; j++)
			printf("%3d\t", tableFrequencyCh[1][i*9+j]);
		printf("\n\n");
		j = 1;

	}		
	return 0;
}