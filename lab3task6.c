#include<stdio.h>
#include<malloc.h>
#include<stdlib.h>
#include<time.h>

// The function fills a array with random numbers 1...4000
void fillARRrand(int* arr, int N)
{
	int i = 0;
	srand((int)time(NULL));
	for (; i < N; i++)
	{
		arr[i] = rand() % 4000+1;
		printf("%d ", arr[i]);
	}
	return;
}

void swap(int* i, int *j)
{
	*i +=*j;
	*j = *i - *j;
	*i -= *j;
	return;
}

void searchMinMaxValue(int *arr, int N, int* imin, int* imax)
{
	int tempMin = 0, tempMax = 0, i;
	int minNumb = arr[0], maxNumb = arr[0];

	for (i = 1; i < N; i++)
	{
		if (arr[i] > maxNumb)
		{
			maxNumb = arr[i];
			tempMax = i;
		}
		if (arr[i] <= minNumb)
		{
			minNumb = arr[i];
			tempMin = i;
		}
	}
	*imin = tempMin;
	*imax = tempMax;
	return;
}

int sumbetweenMinMax(int* arr, int imin, int imax)
{
	int i = 0;
	int sum = 0;
	if (imin > imax)
		swap(&imin, &imax);
	for (i = imin + 1; i < imax; i++)
		sum += arr[i];
	return sum;
}


int main()
{
	int N = 0;
	int *arr;
	int maxNumb = 0, imaxNumb = 0;
	int minNumb = 0, iminNumb = 0;
	int i = 0;
	int sum = 0;

	printf("Enter size of array(max 100): ");
	if (!scanf_s("%d", &N) || (N < 1 || N>100))
	{
		puts("Incorret date!");
		return 1;
	}

	arr = (int*)malloc(N*sizeof(int));
	if (!arr)
	{
		puts("Memory allocation error!");
		exit(1);
	}

	fillARRrand(arr, N);
	
	searchMinMaxValue(arr, N, &iminNumb, &imaxNumb);

	printf("\nSum = %d\n", sumbetweenMinMax(arr, iminNumb, imaxNumb));
	free(arr);
	return 0;
}