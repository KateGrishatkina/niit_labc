#include<stdlib.h>
#include<string.h>
#include<stdio.h>
#include"compressor.h"

void Coder(FILE* fin, FILE* fmiddle, SYM* syms, short int tableCount)
{
	int i = 0;
	unsigned char tempCh;
	rewind(fin);
	rewind(fmiddle);
	while (!feof(fin))
	{
		tempCh = getc(fin);
		for (i = 0; i < tableCount; i++)
		{
			if (tempCh == syms[i].ch)
			{
				fputs(syms[i].code, fmiddle);
				break;
			}
		}
	}
}

void funcPackager(FILE* fmiddle, FILE* fout, header headFout)
{
	int i = 0;
	unsigned char ch;
	unsigned char tempch;
	unsigned char sBYTE[BYTE];
	rewind(fout);
	rewind(fmiddle);
	writeTitle(fout, headFout);
	if (!feof(fmiddle)) 
		ch = getc(fmiddle);
	do
	{		
		if (i < BYTE)
		{
			sBYTE[i] = ch;
			i++;
			ch = getc(fmiddle);
		}
		else
		{
			i = 0;
			tempch = pack(sBYTE);
			fwrite(&tempch, sizeof(tempch), 1, fout);
		}
	} while (!(feof(fmiddle)));
	//if sizeof(fmiddle)%8 != 0
	if (i != BYTE)
	{
		for (; i < BYTE; i++)
			sBYTE[i] = '0';
		tempch = pack(sBYTE);
		fwrite(&tempch, sizeof(tempch), 1, fout);
	}
}

void writeTitle(FILE* file, header head)
{
	int i = 0;
	fwrite(head.signature, sizeof(char), 5, file);
	fwrite(&head.countChInTable, sizeof(head.countChInTable), 1, file);
	for (; i < head.countChInTable; i++)
	{
		fwrite(&head.syms[i].ch, sizeof(head.syms[i].ch), 1, file);
		fwrite(&head.syms[i].freq, sizeof(head.syms[i].freq), 1, file);
	}
	fwrite(&head.lenTail, sizeof(head.lenTail), 1, file);
	fwrite(&head.sizeofFin, sizeof(head.sizeofFin), 1, file);
	fwrite(head.extensionFin, sizeof(char), maxlenEx, file);
}

unsigned char pack(unsigned char* buf)
{
	int i = 0;
	union CODE code;
	code.byte.b1 = buf[0] - '0';
	code.byte.b2 = buf[1] - '0';
	code.byte.b3 = buf[2] - '0';
	code.byte.b4 = buf[3] - '0';
	code.byte.b5 = buf[4] - '0';
	code.byte.b6 = buf[5] - '0';
	code.byte.b7 = buf[6] - '0';
	code.byte.b8 = buf[7] - '0';
	return code.ch;
}

int getExtensionFile(char* direction, char* Extension, short int lenEx)
{
	int i = 0;
	char* lastpoint = NULL;
	while (*direction)
	{
		if (*direction == '.')
			lastpoint = direction;
		direction++;
	}
	while (lastpoint && i < lenEx)
	{
		*Extension = *lastpoint;
		Extension++;
		lastpoint++;
		i++;
	}
	if (i < lenEx) *Extension = '0';
	return i++;
}

long int sizeofFile(FILE *file)
{
	long int size;
	long int currentPosition = ftell(file);
	fseek(file, 0, SEEK_END);
	size = ftell(file);
	fseek(file, currentPosition, SEEK_SET);
	return size;
}


