#ifndef _COMPRESSOR_H_
#define _COMPRESSOR_H_
#include <stdio.h>
#define MAX_LEN 256 
#define FMIDLLE "middle.101"
#define sign "cmpr"
#define maxlenEx 10
#define maxlenSign 5
#define BYTE 8



struct structSYM
{
	unsigned char ch;
	float freq;
	char code[MAX_LEN];
	struct structSYM *left;
	struct structSYM *right;
};

typedef struct structSYM SYM;

typedef struct
{
	char signature[maxlenSign];
	short int countChInTable;
	SYM *syms;
	int lenTail;
	long int sizeofFin;
	char extensionFin[maxlenEx];
} header;

union CODE {
	unsigned char ch;
	struct {
		unsigned short b1 : 1;
		unsigned short b2 : 1;
		unsigned short b3 : 1; 
		unsigned short b4 : 1;
		unsigned short b5 : 1;
		unsigned short b6 : 1;
		unsigned short b7 : 1;
		unsigned short b8 : 1;
	} byte;
};

//main
void funcCompressor(char* finName, char* foutName);

//analyser
void swap(SYM* symsi, SYM* symsj);
void quickSort(SYM* syms, int left, int right);
void fillNewStruct(char newch, SYM* syms);
short int fillTableFreqCh(FILE* file, SYM* syms);

// code generator
void makeCodes(SYM *root);
void printTree(SYM* root);
//void delTree(SYM *root);
SYM* buildTree(SYM** psym, int N);
void codeGenerator(SYM* syms, short int count);

//packager
int getExtensionFile(char* direction, char* Extension, short int lenEx);
long int sizeofFile(FILE *file);
unsigned char pack(unsigned char buf[]);
void Coder(FILE* fin, FILE* fmiddle, SYM* syms, short int tableCount);
void funcPackager(FILE* fmiddle, FILE* fout, header headFout);
void writeTitle(FILE* file, header head);

//decompressor
int recoveryOriginalFile(FILE* fin, FILE* foriginal, SYM* root, const header* head);
void funcDecompressor(char* finName, char* foutName);
int readTitle(FILE* file, header* title);
#endif 


