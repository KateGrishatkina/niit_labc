#define _CRT_SECURE_NO_WARNINGS
#include <string.h>
#include <stdlib.h>
#include "compressor.h"

void codeGenerator(SYM* syms, short int count)
{
	SYM* root = NULL;
	int i = 0;
	SYM** psyms = (SYM **)malloc(sizeof(SYM*)*count);

	if (!psyms)
	{
		perror("Memory error!");
		exit(3);
	}

	for (; i < count; i++)
		psyms[i] = &syms[i];

	root = buildTree(psyms, count);
	makeCodes(root);
	//delTree(root);
	free(psyms);
}

SYM* buildTree(SYM** psym, int N)
{
	int i = 0;
	i = N - 2;
	SYM* temp = (SYM*)malloc(sizeof(SYM));
	temp->freq = psym[N - 2]->freq + psym[N - 1]->freq;
	temp->left = psym[N - 1];
	temp->right = psym[N - 2];
	temp->code[0] = 0;
	if (N == 2)
		return temp;
	// ------------------mine----------
	psym[N - 1] = temp;
	for (i = N - 2; i >= 0; i--)
	{
		if (temp->freq>psym[i]->freq)
		{
			psym[i + 1] = psym[i];
			psym[i] = temp;
		}
	}
	//-------------------mine-end-----
	return buildTree(psym, N - 1);
}

void makeCodes(SYM *root)
{
	if (root->left)
	{
		strcpy(root->left->code, root->code);
		strcat(root->left->code, "0");
		makeCodes(root->left);
	}
	if (root->right)
	{
		strcpy(root->right->code, root->code);
		strcat(root->right->code, "1");
		makeCodes(root->right);
	}
}

void printTree(SYM* root)
{
	if (root->left)
		printTree(root->left);

	printf("c%c f%f \n", root->ch,  root->freq);

	if (root->right)
		printTree(root->right);
}

void delTree(SYM *root)
{
	if (root)
	{
		delTree(root->right);
		delTree(root->left);
		free(root);	
	}
}



