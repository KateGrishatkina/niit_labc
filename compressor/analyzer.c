#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include "compressor.h"

void swap(SYM* symsi, SYM* symsj)
{
	char ch = symsi->ch;
	float freq = symsi->freq;
	symsi->ch = symsj->ch;
	symsj->ch = ch;
	symsi->freq = symsj->freq;
	symsj->freq = freq;
}

void quickSort(SYM* syms, int left, int right)
{
	int i = left, j = right;
	float m = syms[(left + right) / 2].freq;
	do
	{
		while ((syms[i].freq > m) && (i < right)) i++;
		while ((syms[j].freq < m) && (j>left)) j--;
		if (i <= j)
		{
			swap(&syms[i], &syms[j]);
			i++;
			j--;
		}
	} while (i <= j);

	if (left < j) quickSort(syms, left, j);
	if (i < right) quickSort(syms, i, right);
}

void fillNewStruct(char newch, SYM* syms)
{
	syms->ch = newch;
	syms->freq = 1;
	syms->code[0] = 0;
	syms->left = NULL;
	syms->right = NULL;
}

short int fillTableFreqCh(FILE* file, SYM* syms)
{
	unsigned char tempCh;
	int count = 0, i = 0, numberofCh=0;

	// fill the first element os syms with the first character of input file
	if (!feof(file))
	{
		fillNewStruct(getc(file), syms);
		count++;
		numberofCh++;
	}

	// fill syms with charaters of file
	while (!feof(file))
	{	
		tempCh = getc(file);
		if (feof(file)) break;
		numberofCh++;
		for (i = 0; i < count && count < MAX_LEN; i++)
			if (syms[i].ch == tempCh)
			{
				syms[i].freq++;
				break;
			}
		if (i == count)
		{
			count++;
			fillNewStruct(tempCh, &syms[i]);
		}
	}

	quickSort(syms, 0, count - 1);

	for (i = 0; i < count; i++)
		syms[i].freq /= numberofCh;

	return count;
}