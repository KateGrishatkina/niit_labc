#define _CRT_SECURE_NO_WARNINGS
#include "compressor.h"
#include <stdlib.h>
#include <string.h>

void funcCompressor(char* finName, char* foutName)
{
	short int count�h;
	FILE *fin = fopen(finName, "rb");
	FILE *fout = fopen(foutName, "wb");
	FILE* fmiddle = fopen(FMIDLLE, "wb+");
	header headFout = { 0 };
	SYM* syms = (SYM*)malloc(sizeof(SYM)*MAX_LEN);
	if (!fin || !fout || !fmiddle)
	{
		perror("File error");
		exit(1);
	}
	if (!syms)
	{
		perror("Memory error");
		exit(2);
	}
	count�h = fillTableFreqCh(fin, syms);
	codeGenerator(syms, count�h);
	Coder(fin, fmiddle, syms, count�h);

	//header signature
	strcpy(headFout.signature, sign);
	//header number of character in table
	headFout.countChInTable = count�h;
	//header pointer to array sims
	headFout.syms = syms;
	//header size of input file
	headFout.sizeofFin = sizeofFile(fin);
	//header extension of input file
	getExtensionFile(finName, headFout.extensionFin, maxlenEx);
	//header length tail filemiddle
	headFout.lenTail = BYTE - sizeofFile(fmiddle) % BYTE;

	funcPackager(fmiddle, fout, headFout);
	free(syms);
	fclose(fin);
	fclose(fout);
	fclose(fmiddle);
	remove(FMIDLLE);
}