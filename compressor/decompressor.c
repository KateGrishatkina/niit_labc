#define _CRT_SECURE_NO_WARNINGS
#include "compressor.h"
#include <stdlib.h>
#include <string.h>

void funcDecompressor(char* finName, char* foutName)
{
	char nameofFILEout[MAX_LEN];
	strcpy(nameofFILEout, foutName);
	int i = 0;
	header title = { 0 };
	FILE* fin = fopen(finName, "rb");
	FILE* fout;
	SYM* root = NULL;
	
	if (!fin)
	{
		perror("File error");
		exit(4);
	}

	if (!readTitle(fin, &title))
	{
		puts("Incorrect signature!");
		exit(6);
	}

	strcat(nameofFILEout, title.extensionFin);
	fout = fopen(nameofFILEout, "wb");
	if (!fout)
	{
		perror("File error");
		exit(4);
	}
	SYM** psyms = (SYM **)malloc(sizeof(SYM*)*title.countChInTable);
	if (!psyms)
	{
		perror("Memory error!");
		exit(7);
	}
	for (i = 0; i < title.countChInTable; i++)
		psyms[i] = &title.syms[i];

	root = buildTree(psyms, title.countChInTable);
	
	if(recoveryOriginalFile(fin, fout, root, &title)) 
		puts("File was decompessed without errors!\n");
	else
		puts("File was decompessed with errors!\n");
	
	free(psyms);
	free(title.syms);
	fclose(fin);
	fclose(fout);
}

int readTitle(FILE* file, header* title)
{
	int i = 0;
	fread(title->signature, sizeof(char), maxlenSign, file);

	if (!strcmp(title->signature, sign))
	{
		fread(&title->countChInTable, sizeof(short int), 1, file);
		title->syms = (SYM*)malloc(sizeof(SYM)*title->countChInTable);
		if (!title->syms)
		{
			perror("Memory error!");
			exit(5);
		}
		for (i = 0; i < title->countChInTable; i++)
		{
			fread(&title->syms[i].ch, sizeof(char), 1, file);
			fread(&title->syms[i].freq, sizeof(float), 1, file);
			title->syms[i].left = NULL;
			title->syms[i].right = NULL;
			title->syms[i].code[0] = 0;
		}
		fread(&title->lenTail, sizeof(int), 1, file);
		fread(&title->sizeofFin, sizeof(long int), 1, file);
		fread(title->extensionFin, sizeof(char), maxlenEx, file);
	}
	else
		return 0;
	return 1;
}

int recoveryOriginalFile(FILE* fin, FILE* foriginal, SYM* root, const header* head)
{
	int i = 0, j = 0;
	SYM* temp = root;
	union CODE tempch;
	int lenWithoutTail = BYTE;
	long int currentPosition = ftell(fin);
	long int sizeFile = sizeofFile(fin);
	long int tillTheEnd = sizeFile - currentPosition;
	for (i = 0; i < tillTheEnd; i++)
	{
		tempch.ch = fgetc(fin);
		if (i == tillTheEnd - 1)
			lenWithoutTail = BYTE - head->lenTail;
		for (j = 0; j < lenWithoutTail; j++)
		{
			switch (j)
			{
			case 0:
				if (tempch.byte.b1) temp = temp->right; 
				else temp = temp->left;
				break;
			case 1:
				if (tempch.byte.b2) temp = temp->right;
				else temp = temp->left;
				break;
			case 2:
				if (tempch.byte.b3)  temp = temp->right;
				else temp = temp->left;
				break;
			case 3:
				if (tempch.byte.b4) temp = temp->right;
				else temp = temp->left;
				break;
			case 4:
				if (tempch.byte.b5) temp = temp->right;
				else temp = temp->left;
				break;
			case 5:
				if (tempch.byte.b6) temp = temp->right;
				else temp = temp->left;
				break;
			case 6:
				if (tempch.byte.b7) temp = temp->right;
				else temp = temp->left;
				break;
			case 7:
				if (tempch.byte.b8) temp = temp->right; 
				else temp = temp->left;
				break;
			}
			if (!temp->left&&!temp->right)
			{
				fputc(temp->ch, foriginal);
				temp = root;
			}
		}
	}
	if (sizeofFile(foriginal) == head->sizeofFin) return 1;
	else return 0;
}



