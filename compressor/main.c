#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "compressor.h"


int main(int argc, char* argv[])
{
	char error_input[] = "Using: compressor.exe key < c (compressor) d (decompressor)> <fullname of input file>";
	if (argc < 3)
	{
		puts(error_input);
		return 1;
	}
	else
	{
		if ('c' == argv[1][0])
			funcCompressor(argv[2], "compressed.cmpr");
		else if ('d' == argv[1][0])
			funcDecompressor(argv[2], "recorvery");
		else
		{
			puts(error_input);
			return 2;
		}
	}
	return 0;
}

//int main()
//{
//   funcCompressor("in.txt", "out.txt");
//   funcDecompressor("out.txt", "1.txt");
//
//		//------------��� ��������-�����----------
//	return 0;
//}