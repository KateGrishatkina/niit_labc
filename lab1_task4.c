#include<stdio.h>
#define ft_to_inch 12.00f
#define inch_to_cm 2.54f

float heightUSAtoEurop(int ft, int in)
{
	return (inch_to_cm*((float)ft*ft_to_inch + (float)in));
}

int main()
{
	//ft - foot(feet),  in - inch(inches)
	int height_ft, height_in;
	char answer;
	while (1)
	{
		puts("Waht is your height in feet and inches (example 5ft 2in)");
		scanf_s("%dft %dinch", &height_ft, &height_in);
		if (height_ft < 0 || (height_in < 0 || height_in >= 12)) puts("Error! Incorrect data!");			
		else printf("Your height in cm: %.1f\n", heightUSAtoEurop(height_ft, height_in));
		fflush(stdin);
		puts("To continue, press C");
		scanf_s("%c", &answer, 1);
		if (answer != 'C') return 0;
	}
	return 0;
}
