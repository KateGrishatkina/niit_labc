#define _CRT_SECURE_NO_WARNINGS

#include<stdio.h>
#include<time.h>

int extra_fibonacciNumber_rec(int n, int prev, int prevprev)
{
	if (0 == n) return prev;
	return extra_fibonacciNumber_rec(--n, prev+prevprev, prev);
}

int fibonacciNumber_rec(int n)
{
	if (1 == n) 
		return 0;
	else if (2 == n) 
		return 1;
	else
		return extra_fibonacciNumber_rec(n-2, 1, 0);
}



int main()
{
	int i = 1;
	long int j = 0;
	int number = 0;
	int member = 0;
	printf("Which member of Fibonacci sequence is calculated(max 40)? ");
	if (!scanf_s("%d", &number) || number < 1 || number>40)
	{
		printf("Error!");
		return 1;
	}

	for (; i <= number; i++)

		printf("number=%d member=%d\n", i, fibonacciNumber_rec(i));



	return 0;
}
