#include<stdio.h>
#include<windows.h>
#define numberG 9.81f

int main()
{
	float height = 0.0;
	int secTime = 0;
	printf("Hello! What height does the bomb fall from? ");
	if (!scanf_s("%f", &height) || height<=0)
	{
		printf("Error! Uncorrect data!");
		return 1;
	}
	while (height > 0)
	{
		printf("t = %02ds h = %.1fm\n", secTime++, height);
		height -= numberG*secTime*secTime / 2;
	}
	printf("\nBABAH !!!\n");
	return 0;
}

