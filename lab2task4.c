#include <stdio.h>
#include<stdlib.h>
#include<time.h.>
#include<string.h>
#include<windows.h>
#define MAXSTR 80

int answerToContinue()
{
	char answer;
	fflush(stdin);
	printf("\nTo continue press C: ");
	scanf_s("%c", &answer, 1);
	if ('C' == answer || 'c' == answer) return 1;
	else return 0;
}

void screenToClean()
{
	char answer;
	fflush(stdin);
	printf("Will the screen be cleaned?(Enter Y or y, if it will be): ");
	scanf_s("%c", &answer, 1);
	if ('Y' == answer || 'y' == answer)
	{
		system("cls");
		puts("This program groups characters of string as follows: characters of the Latin alphabet is at first, characters of numbers is at last.");

	}
	return;
}

void swap(char *variable1 , char* variable2)
{
	*variable1 += *variable2;
	*variable2 = *variable1 - *variable2;
	*variable1 -= *variable2;
	return;
}

char randomCharacter()
{
	char ch = '0';
	//srand((int)time(0));
	switch (rand() % 3)
	{
	case 0:
		ch = rand() % 10 + '0';
		break;
	case 1:
		ch = rand() % ('Z' - 'A') + 'A';
		break;
	case 2:
		ch = rand() % ('z' - 'a') + 'a';
		break;
	}
	return ch;
}
                     
char* stuffString(char* string, int stringLength)
{
	int i = 0;
	for (i; i < stringLength-1; i++)
	{
		string[i] = randomCharacter();
	}
	string[i] = 0;
	return string;
}

char* groupingElementsofString(char* string)
{
	int i=0, j;
	j = strlen(string) - 1;
	while (1)
	{
		while (1)
		{
			if (string[j]  >= '0' && string[j] <= '9') j--;
			else break;
		}
		while (1)
		{
			if (string[i] >= 'a' && string[i] <='z' || string[i] >= 'A' && string[i] <= 'Z') i ++;
			else break;
		}
		if (i<=j) swap(&string[i], &string[j]);
		else break;
	}
	return string;
}

int main()
{
	char str[MAXSTR];
	srand((int)time(0));
	puts("This program groups characters of string as follows: characters of the Latin alphabet is at first, characters of numbers is at last.");

	while (1)
	{
		printf("\nThe string is before grouping:\n%s\n", stuffString(str, MAXSTR));
		printf("\nThis string is after grouping:\n%s\n", groupingElementsofString(str));
		if (!answerToContinue()) break;
		screenToClean();
	}

	getchar();
	return 0;
}