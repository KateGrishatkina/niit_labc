#include<stdio.h>
#include<string.h>
#define STRMAX 256

int checkOverflow(int *currSum, int number)
{
	int sum = *currSum + number;
	if (*currSum > sum)
	{
		puts("Overflow");
		return 1;
	}
	*currSum = sum;
	return 0;
}


int main()
{
	char buf[STRMAX];
	char number[9];
	int inNumb = 0;
	int countNumberLen = 0;
	int sum = 0;
	int i = 0;
	puts("This programm calculates sum of numbers in string. Maximum number of digits is 8.");
	puts("Enter a text(max length is 255 charasters):");
	fgets(buf, STRMAX-1, stdin);
	buf[strlen(buf)] = 0;
	while (buf[i])
	{
		if (buf[i] >= '0'&&buf[i] <= '9')
		{
			if (!inNumb)
				inNumb = 1;
			number[countNumberLen] = buf[i];
			countNumberLen++;
			if (countNumberLen >= 8)
			{
				number[countNumberLen] = 0;
				if (checkOverflow(&sum, atoi(number)))
					return 1;
				countNumberLen = 0;
			}

		}
		else
		{
			if (inNumb)
			{
				inNumb = 0;
				number[countNumberLen] = 0;
				if (checkOverflow(&sum, atoi(number)))
					return 1;
				countNumberLen = 0;
			}
		}
		i++;
	}
	printf("Sum of numbers: %d\n", sum);

	return 0;
}