#define _CRT_SECURE_NO_WARNINGS
#include<stdlib.h>
#include<stdio.h>
#include<time.h>
#include<string.h>
#define STRMAX 256

typedef enum  { FALSE, TRUE } BOOL;

typedef struct
{
	char* pWbeg;
	BOOL flag;
} WORD;

int getWords(char *str, WORD *words)
{
	BOOL inWord = FALSE;
	int count = 0;
	while (*str)
	{
		if (*str != ' '&&inWord == FALSE)
		{
			inWord = TRUE;
			words->pWbeg = str;
			words->flag = FALSE;
			count++;
			words++;
		}
		else if (*str == ' '&&inWord == TRUE)
		{
			inWord = FALSE;
		}

		str++;
	}
	return count;
}

char* printWord(char* str, char* strContainer)
{
	while (*str != ' '&&*str != 0)
		*strContainer++ = *str++;
	*strContainer = ' ';
	strContainer++;
	return strContainer;
}

void createString(WORD * words, int size, char* str)
{
	int i = size;
	int randNumb = -1;
	srand((int)time(NULL));
	while (i)
	{
		randNumb = rand() % size;
		if (words[randNumb].flag == FALSE)
		{
			str = printWord(words[randNumb].pWbeg, str);
			words[randNumb].flag = TRUE;
			i--;
		}
	}
	str--;
	*str = 0;
	return;
}

int processingFile(FILE *finput, FILE *fresult)
{
	WORD words[STRMAX / 2];
	int count = 0;
	char buf[STRMAX];
	char bufresult[STRMAX];
	while (fgets(buf, STRMAX, finput)!=NULL)
	{
		buf[strlen(buf) - 1] = 0;
		count = getWords(buf, words);
		createString(words, count, bufresult);
		fputs(bufresult, fresult);
		fputc('\n', fresult);
		count = 0;
	}
	if (!buf) return 1;
	return 0;
}

int main()
{
	FILE *fin;
	FILE *fout;
	fin = fopen("in.txt", "rt");
	fout = fopen("out.txt", "wt");
	if (!fin || !fout)
	{
		perror("Error opening file");
		exit(1);
	}
	if(!processingFile(fin, fout)) return 1;
	fclose(fin);
	fclose(fout);
	return 0;
}