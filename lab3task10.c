#include<stdio.h>
#include<string.h>
#define MAXSTR 256

int main()
{
	char buf[MAXSTR];
	char* pbuf = buf;
	int inWord = 0, inNWord = 0, EndNWord = 0;
	int wordCounter = 0, lengthNWord = 0;
	int userNumbWord = 0;
	int i = 0;
	int answer;
	puts("This programm deletes Nth word of string.\nEnter your text (its maximum  length is 255 characters):");
	fgets(buf, MAXSTR, stdin);
	buf[strlen(buf)] = 0;
	fflush(stdin);
	printf("Enter a number of word: ");
	if (!scanf_s("%d", &userNumbWord))
	{
		puts("Incorrect date!");
		return 0;
	}

	printf("1 - pointers\n2 - []\nYour answer: ");
	if (!scanf_s("%d", &answer) || answer>=2 && answer<=1 )
		{
			puts("Incorrect date!");
			return 0;
		}
	
	if (answer-1)
	{
		while (buf[i] && !EndNWord)
		{
			if (buf[i] != ' '&& !inWord)
			{
				inWord = 1;
				if (++wordCounter == userNumbWord)
					inNWord = 1;
			}
			else if (buf[i] == ' ' && inWord)
			{
				inWord = 0;
				if (inNWord)
				{
					EndNWord = 1;
					continue;
				}
			}
			if (inNWord) lengthNWord++;
			i++;
		}

		if (!inNWord)
			printf("The string don't have the %dth word", userNumbWord);
		else
		{
			while (buf[i-1])
			{
				buf[i-lengthNWord] = buf[i];
				i++;
			}
			puts(buf);
		}
	}
	else
	{
		while (*pbuf && !EndNWord)
		{
			if (*pbuf != ' '&& !inWord)
			{
				inWord = 1;
				if (++wordCounter == userNumbWord)
					inNWord = 1;
			}
			else if (*pbuf == ' ' && inWord)
			{
				inWord = 0;
				if (inNWord)
				{
					EndNWord = 1;
					continue;
				}
			}
			if (inNWord) lengthNWord++;
			pbuf++;
		}

		if (!inNWord)
			printf("The string don't have the %dth word", userNumbWord);
		else
		{
			while (*(pbuf - 1))
			{
				*(pbuf - lengthNWord) = *pbuf;
				pbuf++;
			}
			puts(buf);
		}
	}
	
	return 0;
}