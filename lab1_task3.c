#include<stdio.h>
# define OneRadianToDegrees 57.29
double RadToDegrees(double radian)
{
	return  OneRadianToDegrees*radian;
}

double DegreesToRad(double degrees)
{
	return (degrees/OneRadianToDegrees);
}

int main()
{
	// 1 radian = 57,29 degrees
	double value=0.0;
	char unit_of_measurement;

	puts("Hello!");
	while (1)
	{
		puts("Enter angle value in degrees or in radians (example 45.00R, 12.05D, 1.25R, R - radians, D - degrees)");
		scanf_s("%lf%c", &value, &unit_of_measurement, 1);
		if (unit_of_measurement == 68)//D
			printf("%.2lfD = %.2lfR\n", value, DegreesToRad(value));
		else if (unit_of_measurement == 82)//R
			printf("%.2lfR = %.2lfD\n", value, RadToDegrees(value));
		else
		{
			puts("Error!");
			return 1;
		}

	}
	return 0;
}