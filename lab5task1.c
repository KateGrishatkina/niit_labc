#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<time.h>
#define STRMAX 256

typedef enum  { FALSE, TRUE } BOOL; 

typedef struct
{
	char* pWbeg;
	BOOL flag;
} WORD;

int enterString(char *str)
{
	puts("Enter your string(max number of character is 255)");
	if(fgets(str, STRMAX, stdin)==NULL) return 0;
	str[strlen(str) - 1] = 0;
	return 1;
}
int getWords(char *str, WORD *words)
{
	BOOL inWord = FALSE;
	int count = 0;
	while (*str)
	{
		if (*str != ' '&&inWord == FALSE)
		{
			inWord = TRUE;
			words->pWbeg = str;
			count++;
			words++;
		}
		else if (*str == ' '&&inWord == TRUE)
		{
			inWord = FALSE;
		}
		
		str++;
	}
	return count;
}

char* printWord(char* str, char* strContainer)
{
	while (*str != ' '&&*str != 0)
		*strContainer++ = *str++;
	*strContainer = ' ';
	strContainer++;
	return strContainer;
}

void createString(WORD * words, int size, char* str)
{
	int i = size;
	int randNumb=-1;
	srand((int)time(NULL));
	while (i)
	{
		randNumb = rand() % size;
		if (words[randNumb].flag == FALSE)
		{
			str = printWord(words[randNumb].pWbeg, str);
			words[randNumb].flag = TRUE;
			i--;
		}
	}
	str--;
	*str = 0;
	return;
}

int main()
{
	char userString[STRMAX], processedString[STRMAX];
	WORD words[STRMAX / 2] = { { NULL, FALSE } };
	int numbWords;
	if (!enterString(userString)) return 1;
	numbWords = getWords(userString, words);
	if (!numbWords) return 2;
	createString(words, numbWords, processedString);
	puts(processedString);
	return 0;
}