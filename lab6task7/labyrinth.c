#define _CRT_SECURE_NO_WARNINGS
#include"labyrinth.h"
#include<stdio.h>
#include<stdlib.h>
#include<windows.h>

void FillArray(char(*arr)[TOPLBorder])
{
	char buf[TOPLBorder + 2];
	int i = 0, j = 0;
	FILE* flab = fopen("labyrinth.txt", "rt");
	if (!flab)
	{
		perror("File error!");
		exit(1);
	}
	while (fgets(buf, TOPLBorder + 2, flab))
	{
		if (i < TOPWBorder)
			for (j = 0; j < TOPLBorder; j++)
				arr[i][j] = buf[j];
		i++;
	}
	fclose(flab);
}

void printArray(char(*arr)[TOPLBorder])
{
	system("cls");
	int i = 0, j = 0;
	for (i = 0; i < TOPWBorder; i++)
	{
		for (j = 0; j < TOPLBorder; j++)
			printf("%c", arr[i][j]);
		printf("\n");
	}
}


int exitLabyrinth_extra(char(*labyrinth)[TOPLBorder], int i, int j)
{
	Sleep(200);
	if (i == BOTTOMBorder || j == TOPLBorder - 1 || j == BOTTOMBorder || i == TOPWBorder - 1)
	{
		printf("Dobby is free!!!\n");
		exit(1);
	}

	if (labyrinth[i - 1][j] == ' ')
	{
		labyrinth[i][j] = '0';
		labyrinth[i - 1][j] = 'X';
		printArray(labyrinth);
		exitLabyrinth_extra(labyrinth, i - 1, j);
	}
	if (labyrinth[i + 1][j] == ' ')
	{
		labyrinth[i][j] = '0';
		labyrinth[i + 1][j] = 'X';
		printArray(labyrinth);
		exitLabyrinth_extra(labyrinth, i + 1, j);
	}
	if (labyrinth[i][j + 1] == ' ')
	{
		labyrinth[i][j] = '0';
		labyrinth[i][j + 1] = 'X';
		printArray(labyrinth);
		exitLabyrinth_extra(labyrinth, i, j + 1);
	}
	if (labyrinth[i][j - 1] == ' ')
	{
		labyrinth[i][j] = '0';
		labyrinth[i][j - 1] = 'X';
		printArray(labyrinth);
		exitLabyrinth_extra(labyrinth, i, j - 1);
	}
}
void exitLabyrinth(char(*labyrinth)[TOPLBorder])
{
	int x = 0, y = 0;
	int i = 0, j = 0;
	for (i = 0; i < TOPWBorder; i++)
	{
		for (j = 0; j < TOPLBorder; j++)
			if ('X' == labyrinth[i][j])
			{
				x = j;
				y = i;
			}
	}
	exitLabyrinth_extra(labyrinth, y, x);
}
