#include<stdio.h>
#include<malloc.h>
#include<string.h>
#include<stdlib.h>
#define MaxName 80

int YoungOldMembers(char** members, int N, char **young, char **old)
{

	int agedYoung = 1000;
	int agedOld = -1;
	int aged = -1;
	int i = 0;
	for (; i < N; i++)
	{
		printf("Enter name your member(max number of character is 79): ");
		fflush(stdin);
		if (fgets(members[i], 80, stdin) == NULL)
		{
			puts("Error of reading date");
			return 1;
		}
		members[i][strlen(members[i])-1] = '\0';
		members[i][79] = '\0';
		while (1)
		{
			fflush(stdin);
			printf("Enter member's aged: ");
			if (scanf_s("%d", &aged) && aged >= 0 && aged < 200) break;
			else puts("Incorrect date!");
		}
		if (aged < agedYoung)
		{
			*young = members[i];
			agedYoung = aged;
		}
		if (aged > agedOld)
		{
			*old = members[i];
			agedOld = aged;
		}
	}
	return 0;
}

int main()
{
	char mes[][30] = { "Memory allocation error!", "members of your family" };
	int N=0, i=0;
	char** members;
	char* young=NULL;
	char* old=NULL;
	while (1)
	{
		fflush(stdin);
		puts("How many members are in your family? ");
		if (scanf_s("%d", &N)&&N>0) break;
		else puts("Incorrect date!");
	}
	members = (char **)malloc(N*sizeof(char*));
	if (!members)
	{
		puts(mes[0]);
		exit(1);
	}
	for (; i < N; i++)
	{
		members[i] = (char*)malloc(MaxName*sizeof(char));
		if (!members[i])
		{
			puts(mes[0]);
			exit(1);
		}
	}

	
	if (YoungOldMembers(members, N, &young, &old))
	{
		for (i = 0; i < N; i++)
			free(members[i]);
		free(members);
		return 1;
	}

	printf("The youngest %s: %s\nThe odlest %s: %s\n", mes[1], young, mes[1], old);
	
	for (i = 0; i < N; i++)
		free(members[i]);
	free(members);
	return 0;
}