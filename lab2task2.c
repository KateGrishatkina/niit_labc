#include <stdio.h>
#include<stdlib.h>
#include<time.h.>
#define MAXNAME 80

int answerToContinue()
{
	char answer;
	fflush(stdin);
	printf("\nTo continue press C: ");
	scanf_s("%c", &answer, 1);
	if ('C' == answer || 'c' == answer) return 1;
	else return 0;
}

int dataInputValidation(int *number)
{
	printf("\nNumber = ");
	fflush(stdin);
	if (!scanf_s("%d", number) || *number < 0 || *number > 100)
	{
		printf("Error! Uncorrect data! ");
		if (answerToContinue()) dataInputValidation(number);
		else return 1;
	}
	else return 0;
}

int main()
{
	char name[MAXNAME];
	char message[] = "Try again!";
	int number=-1;
	int randNumber=-1;
	printf("Hello! What is your name? ");
	scanf_s("%s", name, MAXNAME - 1);
	while (1)
	{
		srand((int)time(0));
		randNumber = rand() % 100 + 1;
		
		while (1)
		{
			printf("%s,  I have thought a number from 1 to 100, guess it! ", name);
			if (dataInputValidation(&number)) return 0;
			else break;
		}
		
		while (number != randNumber)
		{
			if (number > randNumber)
			{
				printf("Larger! %s, %s ", name, message);
				dataInputValidation(&number);
			}
			else
			{
				printf("Less! %s, %s ", name, message);
				dataInputValidation(&number);
			}
		}
		printf("You guess!");
		if (answerToContinue()) continue;
		else return 0;
	}
	return 0;
}