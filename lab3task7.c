#include<stdio.h>
#include<string.h>
#define MAXSTR 256

struct deskCharacter {
	char character;
	int occurrence;
};


void swap(struct deskCharacter* tablei, struct deskCharacter* tablej)
{
	int occ = tablei->occurrence;
	tablei->occurrence = tablej->occurrence;
	tablej->occurrence = occ;
	char ch = tablei->character;
	tablei->character = tablej->character;
	tablej->character = ch;
}


void quickSort(struct deskCharacter* table, int left, int right)
{	
	int i = left, j = right;
	int m = table[(left + right) / 2].occurrence;
	do
	{
		while ((table[i].occurrence > m) && (i < right)) i++;
		while ((table[j].occurrence < m) && (j>left)) j--;
		if (i <= j)
		{
			swap(&table[i], &table[j]);
			i++;
			j--;
		}
	} while (i <= j);

	if (left < j) quickSort(table, left, j);
	if (i < right) quickSort(table, i, right);
}



int parsStr(const char *str, struct deskCharacter* table)
{
	int countCharacter = 0;
	char *pstr = str;
	struct deskCharacter *ptable = table;
	while (*pstr)
	{
		while (ptable->character)
		{
			if (*pstr == ptable->character)
			{
				ptable->occurrence++;
				break;
			}
			ptable++;
		}
		if (!ptable->character)
		{
			ptable->character = *pstr;
			ptable->occurrence++;
			countCharacter++;
		}
		ptable = table;
		pstr++;
	}
	printf("countOcc %d\n", countCharacter);
	ptable = table;
	while (ptable->character)
	{
		printf("ch %c occ %d\n", ptable->character, ptable->occurrence);
		ptable++;

	}
	printf("\n");
	return countCharacter;
}


int main()
{
	char buf[MAXSTR];
	int numberOfCharacter = 0;
	struct deskCharacter tableOccurence[256] = { { 0, 0 } };
	struct deskCharacter *ptableOccurence = tableOccurence;
	puts("Enter a text(max length is 256 charasters):");
	fgets(buf, MAXSTR, stdin);
	buf[strlen(buf) - 1] = 0;
	quickSort(tableOccurence, 0, parsStr(buf, tableOccurence) - 1);
	while (ptableOccurence->character)
	{
		printf("ch %c occ %d\n", ptableOccurence->character, ptableOccurence->occurrence);
		ptableOccurence++;

	}
	return 0;
}