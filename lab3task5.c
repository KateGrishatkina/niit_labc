#include<stdio.h>
#include<malloc.h>
#include<stdlib.h>
#include<time.h>

// The function fills a array with random numbers -2000...2000
void fillARRrand(int* arr, int N)
{
	int i=0;
	srand((int)time(NULL));
	for (; i < N; i++)
	{
		arr[i] = rand() % 4001 - 2000;
		printf("%d ", arr[i]);
	}
	return;
}

void swap(int* i, int *j)
{
	*i += *j;
	*j = *i - *j;
	*i += - *j;
}




int main()
{
	int N=0;
	int *arr;
	int firstNegNumb = 0, flagNegNumb = 0;
	int lastPosNumb = 0, flagPosNumb = 0;
	int i=0, j=N;
	int sum=0;
	
	printf("Enter size of array(max 100): ");
	if (!scanf_s("%d", &N) || (N < 1 || N>100))
	{
		puts("Incorret date!");
		return 1;
	}
	
	arr = (int*)malloc(N*sizeof(int));
	if (!arr)
	{
		puts("Memory allocation error!");
		exit(1);
	}
	
	fillARRrand(arr, N);
	
	for (i=0, j=N; i < N && (!flagNegNumb ||!flagPosNumb); i++, j--)
	{
		if (!flagNegNumb && arr[i] < 0)
		{
			flagNegNumb = 1;
			firstNegNumb = i;
		}
		if (!flagPosNumb && arr[j] > 0)
		{
			flagPosNumb = 1;
			lastPosNumb = j;
		}
	}
	
	if (firstNegNumb > lastPosNumb)
		swap(&firstNegNumb, &lastPosNumb);
	for (i = firstNegNumb+1; i < lastPosNumb; i++)
		sum += arr[i];
	//warning: firstNegNumb and lastPosNumb could swap() in this point the programm
	printf("\nSum = %d\n", sum);
	
	free(arr);
	return 0;
}