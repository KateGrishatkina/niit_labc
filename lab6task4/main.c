#include <stdio.h>
#include<stdlib.h>
#include"lab6task4.h"
#define _CRT_SECURE_NO_WARNINGS

int main(int argc, char* argv[])
{
	int degree = 0;
	if (argc < 2)
	{
		puts("Using: lab6task4.exe <degree of 2(max 10)>");
		return 1;
	}
	else
	{
		degree = atoi(argv[1]);
		if (degree > 10 || degree < 2)
		{
			puts("Using: lab6task4.exe <degree of 2(max 10)>");
			puts("Error!");
			return 1;
		}
	}
	switch (processArray(pow(BASE, degree)))
	{
	case 0:
		puts("The traditional way is faster!");
		break;
	case 1:
		puts("Ways have the same speed");
		break;
	case 2:
		puts("The recursive way is faster!");
	}
	return 0;
}