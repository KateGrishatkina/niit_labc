#ifndef _LAB6TASK4_H_
#define _LAB6TASK4_H_
#define _CRT_SECURE_NO_WARNINGS
#define BASE 2

typedef struct
{
	unsigned int time_start;
	unsigned int time_finish;
} TIME;

int pow(int base, int exp);
int processArray(int size); // ���������� 0 � ������, ���� ������� ������., 1 � ������, ���� �����������
void fillArray(int *arr, int size);
int sumItemsArr_tr(int *arr, int size);
int sumItemsArr_rec(int *arr, int size);
int extra_sumItemsArr_rec(int *arr, int size, int sum);

#endif _LAB6TASK4_H_