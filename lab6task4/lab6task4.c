#include<stdlib.h>
#include<time.h>
#include"lab6task4.h"
int pow(int base, int exp)
{
	if (exp == 0)
		return 1;
	else
		return base*pow(base, exp - 1);
}
int processArray(int size)// return 0 if traditional is faster, return 1 if ways is equal, return 2 recursive way is faster
{
	int i = 0;
	TIME runtime[2];
	int *arr = (int*)malloc(sizeof(int)*size);
	fillArray(arr, size);	
	runtime[0].time_start = clock();
	sumItemsArr_tr(arr, size);
	runtime[0].time_finish = clock();
	runtime[1].time_start = clock();
	sumItemsArr_rec(arr, size);
	runtime[1].time_finish = clock();
	free(arr);
	if ((runtime[0].time_finish - runtime[0].time_start) < (runtime[1].time_finish - runtime[1].time_start)) 
		return 0;
	else 
		if ((runtime[0].time_finish - runtime[0].time_start) == (runtime[1].time_finish - runtime[1].time_start)) 
			return 1;
		else return 2;
}

void fillArray(int *arr, int size)
{
	while (size)
	{
		*arr = rand() % 100;
		arr++;
		size--;
	}
}
int sumItemsArr_tr(int *arr, int size)
{
	int sum = 0;
	while (size)
	{ 
		sum += *arr;
		arr++;
		size--;
	}
	return sum;
}
int sumItemsArr_rec(int *arr, int size)
{
	if (1 == size)
		return *arr;
	else 
		return (*arr + sumItemsArr_rec(--arr, --size));
}


