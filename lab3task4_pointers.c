#include<stdio.h>
#include<string.h>
#define STRMAX 256

int stringToInt(char* lastElem, int len)
{
	char* LE = lastElem;
	int numb = 0;
	int factor = 1;
	while (len)
	{
		numb += factor*(int)(*LE - '0');	
		factor *= 10;
		LE--;
		len--;
	}
	return numb;
}

int checkOverflow(int *currSum, int number)
{
	int sum=*currSum+number;
	if (*currSum > sum)
	{
		puts("Overflow");
		return 1;
	}
	*currSum = sum;
	return 0;	
}


int main()
{
	char buf[STRMAX];
	char* pbuf=buf;
	int inNumb = 0;
	int countNumberLen=0;
	int sum = 0;
	puts("This programm calculates sum of numbers in string. Maximum number of digits is 8.");
	puts("Enter a text(max length is 255 charasters):");
	fgets(buf, STRMAX-1, stdin);
	buf[strlen(buf)] = 0;
	while (*pbuf)
	{
		if (*pbuf >= '0'&&*pbuf <= '9')
		{
			if (!inNumb)
				inNumb = 1;
			countNumberLen++;
			if (countNumberLen == 8)
			{
				if (checkOverflow(&sum, stringToInt(pbuf, 8)))
					return 1;
				countNumberLen = 0;
			}

		}
		else
		{
			if (inNumb)
			{
				inNumb = 0;
				if (checkOverflow(&sum, stringToInt(pbuf - 1, countNumberLen)))
					return 1;
				countNumberLen = 0;
			}
		}
		pbuf++;
	}
	printf("Sum of numbers: %d\n", sum);

	return 0;
}