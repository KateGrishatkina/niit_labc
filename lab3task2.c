#include<stdio.h>
#include<string.h>
#define MAXSTR 256

int main()
{
	char str[MAXSTR];
	char* pstr = str;
	int wordCounter = 0, inWord = 0, lengthWord=0;
	puts("Enter your text (its maximum  length is 256 characters):");
	fgets(str, MAXSTR, stdin);
	str[strlen(str) - 1] = 0;

	while (*pstr)
	{
		if (*pstr != ' ')
		{
			putchar(*pstr);
			lengthWord++;
			if (!inWord)
			{
				inWord = 1;
				wordCounter++;
			}
			
		}
		else if (inWord)
		{
			inWord = 0;
			printf(" %d\n", lengthWord);
			lengthWord = 0;
		}
		pstr++;
	}
	if (inWord)
		printf(" %d\n", lengthWord);

	printf("Number of word %d\n", wordCounter);
	return 0;
}