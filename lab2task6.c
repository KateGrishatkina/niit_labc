#include<stdio.h>
#define MAXSTR 256

int numberOfUnnecessarySpaces(char *secondSpace)
{
	char *Space = secondSpace;
	int number;
	Space++;
	if (*Space!= 32) return 1;
	number = 1 + numberOfUnnecessarySpaces(Space);
	return number;
}

int main()
{
	char string[MAXSTR];
	int i = 0;
	int numbOfExtraSpaces = 0;
	int shiftCounter=0;
	puts("Enter text (maximum string length is 256 characters):");
	gets_s(string, MAXSTR-1);
	if (' ' == string[0])
	{
		shiftCounter = numberOfUnnecessarySpaces(&string[0]);
	}
	else shiftCounter = 0;
	i = shiftCounter;
	do
	{
		string[i - shiftCounter] = string[i];
		if (' ' == string[i] && ' ' == string[i + 1])
		{
			numbOfExtraSpaces = numberOfUnnecessarySpaces(&string[i + 1]);
			i += numbOfExtraSpaces;
			shiftCounter += numbOfExtraSpaces;

		}	
		i++;
	} while (string[i]);
	string[i - shiftCounter] = string[i];
	puts(string);
	return 0;
}