int getInputData(void);
int DrawKaleidoscope(int);
void ClearMatrix(char**, int);
void PrintMatrix(char**, int);
void FillMatrix(char**, int);
void RandomFillMatrix(char**, int);
#define timeSleep 200
#define numbImage 500

