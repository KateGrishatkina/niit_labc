#include<stdio.h>
#include<malloc.h>
#include<windows.h>
#include<stdlib.h>
#include<time.h>
#include "lab5task2.h"



int main()
{
	int size;
	if (!(size = getInputData()))
	{
		puts("Error");
		return 1;
	}
	srand((int)time(NULL));
	if (!DrawKaleidoscope(size))
	{
		puts("Error");
		return 2;
	}
	return 0;
}

int getInputData()
{
	int size=0;
	puts("This program draws the kaleidoscope image on display.");
	printf("Enter the size of matrix (from 3 to 80): ");
	if (!scanf_s("%d", &size)) return 0;
	if (size >2 && size <=80) return size;
	else if (size < 3) return 3;
	else return 80;	
}

int DrawKaleidoscope(int size)
{
	int i = 0;
	char **kaleidoscope = NULL;
	kaleidoscope = (char**)malloc(sizeof(char*)*size);
	if (!kaleidoscope) return 0;
	for (; i < size; i++)
	{
		kaleidoscope[i] = (char*)malloc(sizeof(char)*size);
		if (!kaleidoscope) return 0;
	}

	for (i = 0; i < numbImage; i++)
	{
		system("cls");
		ClearMatrix(kaleidoscope, size);
		FillMatrix(kaleidoscope, size);
		PrintMatrix(kaleidoscope, size);
		Sleep(timeSleep);
	}

	for (i = 0; i < size; i++)
		free(kaleidoscope[i]);
	free(kaleidoscope);
	return 1;
}

void ClearMatrix(char** matrix, int size)
{
	int i = 0, j = 0;
	for (; i < size; i++)
		for (j = 0; j < size; j++)
			matrix[i][j] = ' ';
}

void PrintMatrix(char** matrix, int size)
{
	int i = 0, j = 0;
	for (; i < size; i++)
	{
		for (j = 0; j < size; j++)
			printf("%c ", matrix[i][j]);
		printf("\n");
	}
	printf("\n");
}

void FillMatrix(char** matrix, int size)
{
	int i = 0, j = 0, z = 0;
	int halfsize = 0;
	if(size%2) halfsize=size/2+1;
	else halfsize = size / 2;
	RandomFillMatrix(matrix, halfsize);
	//the rigth top quadrant
	for (i = 0; i < halfsize; i++)
		for (j = 0, z = size - 1; j < z; j++, z--)
			matrix[i][z] = matrix[i][j];
	//the bottom part
	for (i = 0, z = size - 1; i < z; i++, z--)
		for (j = 0; j < size; j++)
			matrix[z][j] = matrix[i][j];
}

void RandomFillMatrix(char** matrix, int size)
{
	int i = 0, j = 0;
	for (; i < size; i++)
		for (j = 0; j < size; j++)
		{
			if (rand() % 2) matrix[i][j] = '*';
		}
}



