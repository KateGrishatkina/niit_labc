#include "lab5task3.h"

int main()
{
	char fin[STRMAX], fout[STRMAX];
	if (inputDate(fin, fout))
	{
		strncpy(fin, "in.txt", STRMAX);
		strncpy(fout, "out.txt", STRMAX);
	}
	srand((int)time(NULL));
	processFile(fin, fout);
	return 0;
}