#include "lab5task3.h"

int inputDate(char* filenamein, char* filenameout)
{
	char answer;
	printf("Would you like enter the in out filename?(Y or N): ");
	scanf("%c", &answer);
	if (answer == 'N') return 1;
	else
	{
		printf("Enter the filename, what contains input data: ");
		scanf("%s", filenamein);
		printf("Enter the output filename: ");
		scanf("%s", filenameout);
	}
	return 0;
}
int processFile(char* filenamein, char* filenameout)
{
	char buf[STRMAX];
	char* pbuf = buf;
	char bufout[STRMAX];
	char tempCh;
	BOOL inWord = FALSE;
	int countCh = 0;
	FILE* fin = fopen(filenamein, "rt");
	FILE* fout = fopen(filenameout, "wt");
	if (!fin || !fout)
	{
			perror("File opening error");
			return 1;
	}
	tempCh = fgetc(fin);
	if (tempCh == EOF) return 2;
	else
	{
		do
		{
			if (isspace(tempCh)||countCh==STRMAX)
			{
				if (inWord == TRUE)
				{
					inWord = FALSE;
					*pbuf = '\0';
					processString(buf, bufout);
					pbuf = buf;
					countCh = 0;
					fputs(bufout, fout);
				}
			}
			else
			{
				inWord = TRUE;
				*pbuf = tempCh;
				countCh++;
				pbuf++;
			}
			if(inWord==FALSE )
				fputc(tempCh, fout);
			tempCh = fgetc(fin);
		} while (tempCh != EOF);
	}
	fclose(fin);
	fclose(fout);
	return 0;
}

//int readWordfromFile(FILE *fin, char* str);
void processString(char* strin, char* strout)
{
	*strout = *strin;
	strin++;
	strout++;
	CHARACTER chs[STRMAX];
	CHARACTER *pchs = chs;
	int count = 0, i = 0;
	int randNumb = 0;
	BOOL flagRead = FALSE;

	while (*strin)
	{
		pchs->character = *strin;
		printf("pchs->character = *strin %c\n", pchs->character);
		pchs->isUsed = FALSE;
		strin++;
		pchs++;
		count++;
	}
	printf("count:  %d", count);
	for (i = 0; i < count-1; i++)
	{
		flagRead = FALSE;
		printf("infor\n");
		while (flagRead == FALSE)
		{
			randNumb = rand() % (count-1);
			if (chs[randNumb].isUsed == FALSE)
			{
				chs[randNumb].isUsed = TRUE;
				*strout = chs[randNumb].character;
				flagRead = TRUE;
				printf("2.rand %d\n", randNumb);
				printf("2.pchs->character = *strin %c\n", chs[randNumb].character);
			}
		}
		strout++;
	}
	*strout = *(strin-1);
	strout++;
	*strout = 0;

	
	//strncpy(strout, strin, STRMAX);
}