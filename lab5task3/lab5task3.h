#define _CRT_SECURE_NO_WARNINGS
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>
#define STRMAX 80

typedef enum {FALSE, TRUE} BOOL;

typedef struct
{
	char character;
	BOOL isUsed;
} CHARACTER;

int inputDate(char* filenamein, char* filenameout);
int processFile(char* filenamein, char* filenameout);
int readWordfromFile(FILE *fin, char* str);
void processString(char* strin, char* strout);